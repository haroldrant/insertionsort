/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Objects;

/**
 *
 * @author Harold
 * @param <T>
 */
public class ListaS<T> {
    private Nodo<T> cabeza=null;
    private int tam=0;
    
    public ListaS(){}
    
    public void insertarInicio(T info){
        this.cabeza=new Nodo(info,cabeza);
        this.tam++;
    }
    
    public void insertarFinal(T info){
       if(this.esVacio())
           this.insertarInicio(info);
       else{
           try{
                Nodo<T> ult=this.getPos(this.tam-1);
                ult.setSig(new Nodo(info,null));
                this.tam++;
           }
           catch (Exception e){
               System.err.print(e.getMessage());
           }
       }
    }
    
    public void insertionSortNodo(){
        Nodo<T> tmp=null;
        for(Nodo<T> actual=this.cabeza;actual!=null;){
                Nodo<T> actualSig=actual.getSig();
                if(tmp==null || comparador(tmp.getInfo(),actual.getInfo())>0){
                    actual.setSig(tmp);
                    tmp=actual;
                }
                else{
                    Nodo<T> ordenar=tmp;
                    while(ordenar.getSig()!=null && comparador(ordenar.getSig().getInfo(),actual.getInfo())<0)
                        ordenar=ordenar.getSig();
                    intercambioNodo(ordenar,actual);                    
                }
            actual=actualSig;
        }
        this.cabeza=tmp;        
    }
    
    private void intercambioNodo(Nodo<T> nuevo, Nodo<T> actual){
        actual.setSig(nuevo.getSig());
        nuevo.setSig(actual);
    }
    
    public void insertionSortInfo(){        
        for(Nodo<T> actual=this.cabeza;actual!=null;actual=actual.getSig()){            
            Nodo<T> actualSig=actual.getSig();            
            while(actualSig!=null){
            if(comparador(actual.getInfo(),actualSig.getInfo())>0){
                intercambioInfo(actual,actualSig);
            }
            actualSig=actualSig.getSig();
            }   
        }
    }
    
    private void intercambioInfo(Nodo<T> actual,Nodo<T> actualSig){
        T aux=actualSig.getInfo();;        
        actualSig.setInfo(actual.getInfo());
        actual.setInfo(aux);
    }
    
    public T get(int pos){
        try{
            Nodo<T> nodoPedido=getPos(pos);
            return nodoPedido.getInfo();
        }
        catch(Exception e){
            return null;
        }
    }
    
    public void set(int pos, T infoNuevo){
        try{
            Nodo<T> nodoPedido=getPos(pos);
            nodoPedido.setInfo(infoNuevo);
            
        }
        catch(Exception e){
            System.err.print(e.getMessage());
        }
    }
    
    private Nodo<T> getPos(int pos)throws Exception {
        if(this.esVacio() || pos<0 || pos>=this.tam)
            throw new Exception("Su posición se encuentra fuera de rango");
        Nodo<T> t=this.cabeza;
        while(pos>0){
            t=t.getSig();
            pos--;
        }
    return t;
    }
    
    public int getTam() {
        return tam;
    }
    
    public boolean esVacio(){
        return (this.cabeza==null)&&(this.tam==0);
    }
    
    public T eliminar(int pos){
        if(this.esVacio())
            return null;
        Nodo<T> borrar;
        if(pos==0){
            borrar=this.cabeza;
            this.cabeza=this.cabeza.getSig();
        }
        else{
            try {
                Nodo<T> antes=this.getPos(pos-1);
                borrar=antes.getSig();
                antes.setSig(borrar.getSig());
            } catch (Exception ex) {
                return null;
            }
        }
        borrar.setSig(null);
        this.tam--;
        return borrar.getInfo();
    }
    
    public String toString(){
        String msg="Cabeza-->";
        Nodo<T> x = this.cabeza;
        while(x!=null){
            msg+=x.getInfo().toString()+"-->";
            x=x.getSig();                    
        }
        return msg;
    }
    
    private int comparador(T info1, T info2)
    {
    Comparable c=(Comparable)info1;
    return c.compareTo(info2);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<?> other = (ListaS<?>) obj;
        
        Nodo<?> cab1=this.cabeza;
        Nodo<?> cab2=other.cabeza;
        while(cab1!=null && cab2!=null)
        {
            if(!cab1.getInfo().equals(cab2.getInfo()))
                return false;
            cab1=cab1.getSig();
            cab2=cab2.getSig();
        }
        return cab1==null && cab2==null;
    }
}
